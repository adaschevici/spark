﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sparkware_project
{
    class Application
    {
        static private bool doLoop;

        public Application()
        {
            doLoop = true;
        }

        private void printMenu()
        {
            Console.WriteLine("\n1) New \n");
            Console.WriteLine("2) Continue \n");
            Console.WriteLine("3) Show \n");
            Console.WriteLine("4) End \n");
            Console.WriteLine("5) Exit \n");
        }

        private void printHelp()
        {
            Console.WriteLine("You need to choose a valid option for the application valid options are");
            Console.WriteLine(" new, continue, show, end, exit ");
        }

        private void quit()
        {
            doLoop = false;
        }

        public void mainloop()
        {
            string choice;

            do
            {
                printMenu();
                Console.WriteLine("Choose an option");
                choice = Convert.ToString(Console.ReadLine());
                switch (choice)
                {
                    case "new":
                        {
                            Console.WriteLine("chose new");
                            var game = new Game();
                            game.run();
                            break;
                        }
                    case "continue":
                        {
                            Console.WriteLine("chose continue");
                            var game = new Game();
                            game = game.Load();
                            game.run();
                            break;
                        }
                    case "show":
                        {
                            Console.WriteLine("chose show");
                            try
                            {
                                var game = new Game();
                                game = game.Load();
                                Printer.Print(game.GameBoard);
                            }
                            catch { }
                            break;
                        }
                    case "end":
                        {
                            Console.WriteLine("chose end");
                            var game = new Game();
                            
                            break;
                        }
                    case "exit":
                        {
                            quit();
                            break;
                        }
                    default:
                        {
                            printHelp();
                            break;
                        }
                }
            } while (doLoop);
        }
    }
}
