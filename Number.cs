﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sparkware_project
{
    class Number
    {
        private int nbValue;
        private bool chosen;
        private string rightChoice;
        

        public Number()
        { }

        public Number(int val, bool chose, string rightChoice)
        {
            nbValue = val;
            chosen = chose;
            this.rightChoice = rightChoice;
        }

        public bool Chosen { get; set; }

        public int Value
        {
            get
            {
                return nbValue;
            }
            set
            {
                nbValue = value;
            }
        }

        public string RightChoice
        {
            get {
                return rightChoice;
            }
            set
            {
                rightChoice = value;
            }
        }

    }
}
