﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sparkware_project
{
    [Serializable]
    class Board
    {
        List<List<Number>> gameboard = new List<List<Number>>();

        private List<Number> generateTuple(int rangeStart, int rangeStop)
        {
            List<Number> tuple = new List<Number>();
            
            
            for (int i = 0; i < 3; i++)
            { 
                
                var randomNumber = StaticRandom.Next(rangeStart, rangeStop);
                //Console.WriteLine(tuple.Find(x => x == randomNumber));
                while (tuple.Exists(x => x.Value == randomNumber))
                {
                    randomNumber = StaticRandom.Next(rangeStart, rangeStop);
                }
                tuple.Add(new Number(randomNumber, false, "   "));
            }
            return tuple;

        }
        // number of columns
        public int Rows { 
            get {
                return GenericBoard[0].Count;
                }
             }
        // returns number of columns
        public int Columns
        {
            get
            {
                return GenericBoard.Count;
            }
        }


        public List<List<Number>> GenericBoard { get; set; }

        public Board() {
            
        }


        public Board(int dim)
        {
            for (int i = 0; i < dim; i++)
            {
                List<Number> column = generateTuple(i * 10, i * 10 + 9);
                gameboard.Add(column);
            }
        }
        // indexer
        public Number this[int x, int y]
        {
            get 
            {
                return GenericBoard[x][y];
            }
            set
            {
                GenericBoard[x][y].RightChoice = value.RightChoice;

            }
        }



    }
}
