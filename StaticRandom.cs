﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sparkware_project
{
    class StaticRandom
    {
        private static Random random = new Random();

        public static int Next(int rangeStart, int rangeStop)
        {
            
            var randomNumber = random.Next(rangeStart, rangeStop);
            return randomNumber;
        }
    }
}
