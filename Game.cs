﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace sparkware_project
{
    class Game
    {
        private int round;
        private bool runningGameLoop;
        private List<int> chosenNumbers = new List<int>();
        private Board gameBoard;
        private int credit;

        public int Credit { get; set; }

        public Board GameBoard { get; set; }

        public List<int> ChosenNumbers { get; set; }

        public Game()
        {
            runningGameLoop = true;
        }


        public void chooseWinningNumbers(Board brd)
        {
            
            int rnd;
            for (int i = 0; i < 6; i++)
            {
                rnd = StaticRandom.Next(0, 23);
                while (chosenNumbers.Exists(x => x == rnd))
                    rnd = StaticRandom.Next(0, 23);
                chosenNumbers.Add(GameBoard[rnd/9, rnd % 9]);
            }
            
            
        
        }

        public void printGameHelp()
        {
            Console.WriteLine("Allowed options are numbers between 0-89, end, or exit");
        }

        public void run() 
        {
            string choice;
            do{
                
                choice = Convert.ToString(Console.ReadLine());
                try
                {
                    int num = int.Parse(choice);
                    switch (num/90)
                    {
                        case 0:
                            {
                                Console.WriteLine("chose a number");
                                break;
                            }
                        default:
                            {
                                printGameHelp();
                                break;
                            }
                    }
                }catch (FormatException)
                { 
                    switch (choice)
                    {
                        case "exit":
                        {
                            Console.WriteLine("chose to exit");

                            break;
                        }
                        case "end":
                        {
                            Console.WriteLine("Chose to end the game");
                            quit();
                            break;
                        }
                        default:
                        {
                            printGameHelp();
                            break;
                        }
            
                    }
                }

            }while(runningGameLoop);
        }


        public void quit()
        {
            runningGameLoop = false;
        }


        public Game Load()
        {
            string savedGamePath = System.Configuration.ConfigurationSettings.AppSettings["savegameFilePath"];
            var serializer = new JavaScriptSerializer();
            string gameJsonText = System.IO.File.ReadAllText(savedGamePath);
            var loadedGame = serializer.Deserialize<Game>(gameJsonText);
            return loadedGame;
        }

        public void Save(string configPath) 
        {
            string savedGamePath = System.Configuration.ConfigurationSettings.AppSettings["savegameFilePath"];
            var serializer = new JavaScriptSerializer();
            string gameJson = serializer.Serialize(this);
            System.IO.StreamWriter file = new System.IO.StreamWriter(savedGamePath);
            file.Write(gameJson);
        }
    }
}
